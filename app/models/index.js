const dbConfig = require("../config/db.config.js");
const tutorialModel = require("./tutorial.model.js");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;
db.url = dbConfig.url;

db.tutorials = tutorialModel(mongoose);

module.exports = db;