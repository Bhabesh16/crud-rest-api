const express = require("express");
const cors = require("cors");

const db = require("./app/models");

const tutorialRoutes = require("./app/routes/tutorial.routes");

const PORT = process.env.PORT || 8080;

const app = express();

let corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
    })
    .catch((err) => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });

app.get("/", (req, res) => {
    res.json({
        "message": "Welcome to CRUD Restful Api application",
        "get all Tutorials": "GET api/tutorials",
        "get Tutorial by Id": "GET api/tutorials/:id",
        "add new Tutorial": "POST api/tutorials",
        "update Tutorial by Id": "PUT api/tutorials/:id",
        "remove Tutorial by id": "DELETE api/tutorials/:id",
        "remove all Tutorials": "DELETE api/tutorials"
    });
});

tutorialRoutes(app);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});